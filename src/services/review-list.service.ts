import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Review } from '../model/review.model';
 
@Injectable()
export class ReviewListService {
 
    private reviewListRef = this.db.list<Review>('review-list');
 
    constructor(private db: AngularFireDatabase) { }
 
    getreviewList() {
        return this.reviewListRef;
    }
 
    addreview(review: Review) {
        return this.reviewListRef.push(review);
    }
 
    updatereview(review: Review) {
        return this.reviewListRef.update(review.key, review);
    }
 
    removereview(review: Review) {
        return this.reviewListRef.remove(review.key);
    }
}