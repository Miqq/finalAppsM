export interface Review {
    key?: string;
    title: string;
    content: string;
}