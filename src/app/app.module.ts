import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';


import { HomePage } from '../pages/home/home';

import { AjustesPage } from '../pages/ajustes/ajustes';
import { PerfilPage } from '../pages/perfil/perfil';
import { TabsPage } from '../pages/tabs/tabs';
import { BienvenidaPage } from '../pages/bienvenida/bienvenida';
import { LoginPage } from '../pages/login/login';
import { RegistrarPage } from '../pages/registrar/registrar';
import { ReviewsPage } from '../pages/reviews/reviews';
import { ProfesorPage } from '../pages/profesor/profesor';

import { CursosPage } from '../pages/cursos/cursos';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { HttpModule } from '@angular/http';
import { ReviewListService } from '../services/review-list.service';
import { SettingsProvider } from '../providers/settings/settings';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import { IdiomaPage } from '../pages/idioma/idioma';

  const firebaseAuth= {
  apiKey: "AIzaSyBtk6xecJ7tWqq7_o8Ng_Jy7hYK459mkPs",
  authDomain: "final-apps-m-fb.firebaseapp.com",
  databaseURL: "https://final-apps-m-fb.firebaseio.com",
  projectId: "final-apps-m-fb",
  storageBucket: "final-apps-m-fb.appspot.com",
  messagingSenderId: "941164310649"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,

    AjustesPage,
    PerfilPage,
    BienvenidaPage,
    LoginPage,
    RegistrarPage,
    ReviewsPage,
    ProfesorPage,
    NotificacionesPage,
    IdiomaPage,
    TabsPage,
    CursosPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,

    AjustesPage,
    PerfilPage,
    BienvenidaPage,
    LoginPage,
    RegistrarPage,
    ReviewsPage,
    ProfesorPage,
    NotificacionesPage,
    IdiomaPage,
    TabsPage,
    CursosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    ReviewListService,
    SettingsProvider
  ]
})
export class AppModule {}
