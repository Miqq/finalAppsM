import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reviews',
  templateUrl: 'reviews.html',
})
export class ReviewsPage {
  reviews;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.reviews="misReviews";

   

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewsPage');
  }

}
