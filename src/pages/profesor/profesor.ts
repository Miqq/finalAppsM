import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PaginaTemporalPage } from '../pagina-temporal/pagina-temporal';
/**
 * Generated class for the ProfesorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profesor',
  templateUrl: 'profesor.html',
})
export class ProfesorPage {
  items;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.items = [
      'Samuel Navarro'
    ];
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfesorPage');
  }

  seleccion(){
    this.navCtrl.push('PaginaTemporalPage');
  }

}
