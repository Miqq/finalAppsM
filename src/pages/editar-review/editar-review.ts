import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ReviewListService } from '../../services/review-list.service';
import { Review } from '../../model/review.model';

/**
 * Generated class for the EditarReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-review',
  templateUrl: 'editar-review.html',
})
export class EditarReviewPage {

  review: Review = {
    title: '',
    content: ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private reviewListService: ReviewListService) {
  }

  ionViewDidLoad() {
    this.review = this.navParams.get('review');
  }

}
