import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarReviewPage } from './editar-review';

@NgModule({
  declarations: [
    EditarReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarReviewPage),
  ],
})
export class EditarReviewPageModule {}
