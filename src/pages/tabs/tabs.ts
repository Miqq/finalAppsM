import { Component } from '@angular/core';


import { HomePage } from '../home/home';
import { CursosPage } from '../cursos/cursos';
import { AjustesPage } from '../ajustes/ajustes';
import { PerfilPage } from '../perfil/perfil';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab3Root = CursosPage;
  tab4Root = AjustesPage;
  tab5Root = PerfilPage;

  constructor() {

  }
}
