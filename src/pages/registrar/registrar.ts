import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the RegistrarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registrar',
  templateUrl: 'registrar.html',
})
export class RegistrarPage {

    @ViewChild('username') user;
    @ViewChild('password') password;

  constructor(private alertCtrl: AlertController, private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {

    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrarPage');
  }
  alert(message: string) {
    this.alertCtrl.create({
      title: 'Mensaje',
      subTitle: message,
      buttons: ['OK']
    }).present();
}

  registrar(){
    this.fire.auth.createUserWithEmailAndPassword(this.user.value, this.password.value)
    .then(data => {
      console.log('got data ', data);
      this.alert('Bienvenido!');
      this.navCtrl.setRoot( TabsPage );
      // el usuario inicio sesion
    })
    .catch(error => {
      console.log('A ocurrido un error', error);
      this.alert(error.message);
    });
console.log('Would register user with ', this.user.value, this.password.value);
}

}
