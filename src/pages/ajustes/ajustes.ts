import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { NotificacionesPage } from '../notificaciones/notificaciones';
import { IdiomaPage } from '../idioma/idioma';

/**
 * Generated class for the AjustesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {
 selectedTheme: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, private settings: SettingsProvider) {
  this.settings.getActiveTheme().subscribe(val=>this.selectedTheme=val);
  

  }
  

buttonClick(){
this.navCtrl.push(NotificacionesPage)
}
buttonClick2(){
  this.navCtrl.push(IdiomaPage)
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad AjustesPage');
  }
toggleAppTheme(){
  if (this.selectedTheme == 'dark-theme'){
    this.settings.setActiveTheme('light-theme');
  }else{
    this.settings.setActiveTheme('dark-theme');
  }
}
}
