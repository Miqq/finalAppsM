import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ProfesorPage } from '../profesor/profesor';
/**
 * Generated class for the CursosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cursos',
  templateUrl: 'cursos.html',
})
export class CursosPage {
  items;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = [
      'Aplicaciones moviles'
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CursosPage');
  }

  profesor(){
    this.navCtrl.push(ProfesorPage);
  }

}
