import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AgregarReviewPage } from "../agregar-review/agregar-review";
import { ReviewsPage } from '../reviews/reviews';

import { AngularFireDatabase } from 'angularfire2/database';
import { ReviewListService } from '../../services/review-list.service';
import { Review } from '../../model/review.model';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the PaginaTemporalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pagina-temporal',
  templateUrl: 'pagina-temporal.html',
})
export class PaginaTemporalPage {

  reviewList: Observable<Review[]>

  constructor(private db: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams, private reviewListService: ReviewListService) {

    this.reviewList = this.reviewListService.getreviewList()
      .snapshotChanges()
      .map(
      changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }))
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaginaTemporalPage');
  }


}
