import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaginaTemporalPage } from './pagina-temporal';

@NgModule({
  declarations: [
    PaginaTemporalPage,
  ],
  imports: [
    IonicPageModule.forChild(PaginaTemporalPage),
  ],
})
export class PaginaTemporalPageModule {}
