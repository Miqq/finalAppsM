import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgregarReviewPage } from './agregar-review';

@NgModule({
  declarations: [
    AgregarReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(AgregarReviewPage),
  ],
})
export class AgregarReviewPageModule {}
