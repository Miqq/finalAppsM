import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { ReviewListService } from '../../services/review-list.service';
import { Review } from '../../model/review.model';
import { PaginaTemporalPage } from '../pagina-temporal/pagina-temporal';

/**
 * Generated class for the AgregarReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agregar-review',
  templateUrl: 'agregar-review.html',
})
export class AgregarReviewPage {
  
  review: Review = {
    title: '',
    content: ''
  };

  constructor(public db: AngularFireDatabase, public navCtrl: NavController, public navParams: NavParams, private reviewListService: ReviewListService) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgregarReviewPage');
  }

  addreview(review: Review) {
    this.reviewListService.addreview(review).then(ref => {
      this.navCtrl.setRoot('PaginaTemporalPage');
    })
  }

}
